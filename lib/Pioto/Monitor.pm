# vim: set ft=perl :
use warnings;
use strict;

package Pioto::Monitor;

use YAML;

use Exporter;

our @EXPORT_OK = qw(
    load_config
);

my $DEFAULT_CONF_FILE = 'pioto-monitor.defaults.yml';
my $CONF_FILE = 'pioto-monitor.yml';
my $STATE_FILE = 'pioto-monitor-state.yml';

sub load_config {
    my $conf = YAML::LoadFile($DEFAULT_CONF_FILE);
    my $user_conf;
    if (-r $CONF_FILE) {
        $user_conf = YAML::LoadFile($CONF_FILE);
    }
    foreach my $k1 (keys %$user_conf) {
        foreach my $k2 (keys %{$user_conf->{$k1}}) {
            $conf->{$k1}{$k2} = $user_conf->{$k1}{$k2};
        }
    }
    return $conf;
}

sub load_state {
    my $state;
    if (-r $STATE_FILE) {
        $state = YAML::LoadFile($STATE_FILE);
    }
    return $state;
}

sub save_state {
    my $state = shift;
    YAML::DumpFile($STATE_FILE, $state);
}

