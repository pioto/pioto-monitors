#!/usr/bin/env perl
use warnings;
use strict;

use Test::More;

use Net::Ping::External qw(ping);

use Pioto::Monitor;
my $conf = Pioto::Monitor::load_config();

my @servers = @{$conf->{ping}{servers}};
plan tests => scalar(@servers);

foreach my $server (@servers) {
    ok ping(hostname => $server, timeout => $conf->{ping}{timeout}),
        "ping $server";
}

