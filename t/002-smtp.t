#!/usr/bin/env perl
use warnings;
use strict;

use Test::More;

use Test::SMTP;

use Pioto::Monitor;
my $conf = Pioto::Monitor::load_config();

my @hosts = @{$conf->{smtp}{hosts}};
plan tests => scalar(@hosts) * 2;

foreach my $host (@hosts) {
    my $client = Test::SMTP->connect_ok("Connect to $host", Host => $host, AutoHello => 1);
    SKIP: {
        skip "Couldn't connect to $host", 1 unless $client;
        $client->quit_ok("Quit $host");
    }
}

