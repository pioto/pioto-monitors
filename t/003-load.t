#!/usr/bin/env perl
use warnings;
use strict;

use Test::More tests => 4;

use Unix::Uptime;

use Pioto::Monitor;
my $conf = Pioto::Monitor::load_config();

ok my ($load1, $load5, $load15) = Unix::Uptime->load(), 'Got our load averages';

cmp_ok $load1, '<', $conf->{load}{limit}, "1 minute load average < $conf->{load}{limit}";
cmp_ok $load5, '<', $conf->{load}{limit}, "5 minute load average < $conf->{load}{limit}";
cmp_ok $load15, '<', $conf->{load}{limit}, "15 minute load average < $conf->{load}{limit}";

