#!/usr/bin/env perl
use warnings;
use strict;

use Test::More;

use Test::WWW::Mechanize 1.22;

use Pioto::Monitor;
my $conf = Pioto::Monitor::load_config();

my @urls = @{$conf->{web}{urls}};
plan tests => scalar(@urls) + 1;

ok my $mech = Test::WWW::Mechanize->new(agent => "PiotoMon/0.1"), 'Created Mechanize object';

foreach my $url (@urls) {
    $mech->head_ok($url, "HEAD $url");
}

