#!/usr/bin/env perl
use warnings;
use strict;

use Test::More tests => 2;

use Unix::Uptime;

use Pioto::Monitor;
my $conf = Pioto::Monitor::load_config();
my $state = Pioto::Monitor::load_state();

ok my $uptime = Unix::Uptime->uptime(), 'Got our uptime';

if (!defined $state->{uptime}) {
    $state->{uptime} = $uptime;
}

cmp_ok $uptime, '>=', $state->{uptime}, "System hasn't rebooted";

$state->{uptime} = $uptime;
Pioto::Monitor::save_state($state);

