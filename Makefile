.PHONY: all cron test

all :
	prove -l lib/ -r t/ || true

# Run things with stdout to null. That way we just get email on errors.
cron : sleep
	$(MAKE) all >/dev/null

# For testing... verbose output
test : 
	prove -v -l lib/ -r t/

# Dump a formatted HTML version
html :
	prove -m -Q --formatter=TAP::Formatter::HTML -r t/ -l lib/ > ~/public_html/mon.html || true

# Sleep a random amount of time
sleep :
	perl -e 'sleep int(rand(60 * 10))'
